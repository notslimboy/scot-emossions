﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideColliderEnemy : MonoBehaviour
{
    public Transform sideCollider;

    // Update is called once per frame
    void Update()
    {
        sideCollider.position = transform.position;
    }
}
