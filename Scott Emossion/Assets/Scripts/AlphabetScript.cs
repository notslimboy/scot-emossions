﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphabetScript : MonoBehaviour
{
    public GameManager game;
    public char thisAlphabet;
    public bool collected;
    public bool thisIsLast;

    void Start()
    {
        game = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (!collected)
            {
                game.huruf[game.indexHuruf] = thisAlphabet;

                if (thisIsLast)
                {
                    for (int i = 0; i <= game.indexHuruf; i++)
                    {
                        game.kata[game.indexKata] += game.huruf[i];
                    }

                    game.indexHuruf = 0;
                    thisIsLast = false;
                }
                else if (!thisIsLast)
                {
                    game.indexHuruf++;
                }
            }

            collected = true;
            Destroy(gameObject);
        }
    }
}
