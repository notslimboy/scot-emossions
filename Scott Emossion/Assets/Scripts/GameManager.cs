﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Collective Words")]
    public int indexHuruf;
    public int indexKata;
    public char[] huruf;
    public string[] kata;

    [Header("Objective Words")]
    public string[] kunciJawaban;
}
