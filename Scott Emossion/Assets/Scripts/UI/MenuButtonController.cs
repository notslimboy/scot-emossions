﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuButtonController : MonoBehaviour
{
    // Use this for initialization
    public int index;
    [SerializeField] bool keyDown;
    [SerializeField] int maxIndex;
    public AudioSource audioSource;

    void Start () 
    {
        audioSource = GetComponent<AudioSource>();
    }
	
    // Update is called once per frame
    void Update ()
    {
        CheckIndexPosition();

        if(index == 0 && Input.GetKeyDown(KeyCode.Return)){
            Debug.Log("newgame");
            SceneManager.LoadScene(1);
        }
        else if (index == 1 && Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("option");
            SceneManager.LoadScene(2);
        }
        else if (index == 2 && Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("credit");
            SceneManager.LoadScene(3);
        }
        else if (index == 3 && Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("quit");
            Application.Quit();
        }
    }

    private void CheckIndexPosition()
    {
        if (Input.GetAxis("Vertical") != 0)
        {
            if (!keyDown)
            {
                if (Input.GetAxis("Vertical") < 0)
                {
                    if (index < maxIndex)
                    {
                        index++;
                    }
                    else
                    {
                        index = 0;
                    }
                }
                else if (Input.GetAxis("Vertical") > 0)
                {
                    if (index > 0)
                    {
                        index--;
                    }
                    else
                    {
                        index = maxIndex;
                    }
                }

                keyDown = true;
            }
        }
        else
        {
            keyDown = false;
        }
    }
    
}
