﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject BumiMerah;
    public GameObject BumiBiru;
    public GameObject BumiKuning;
    public GameObject BumiHijau;
    public GameObject BumiPink;

    public GameObject CenterCanvas;
    public GameObject RightCanvas;
    public GameObject LeftCanvas;

    public Button rightArrow;
    public Button leftArrow;

    public int levelChosen = 1;
    public float speedMove;

    public bool rightChecked;
    public bool leftChecked;

    // Start is called before the first frame update
    void Start()
    {
        BumiBiru.transform.position = RightCanvas.transform.position;
        BumiHijau.transform.position = RightCanvas.transform.position;
        BumiKuning.transform.position = RightCanvas.transform.position;
        BumiPink.transform.position = RightCanvas.transform.position;

        rightArrow.onClick.AddListener(right);
        leftArrow.onClick.AddListener(left);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            right();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            left();
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            switch (levelChosen)
            {
                case 1:
                    SceneManager.LoadScene(4);
                    break;

                case 2:
                    break;

                case 3:
                    break;

                case 4:
                    break;

                case 5:
                    break;
            }
        }

        if (rightChecked)
        {
            switch (levelChosen)
            {
                case 1:
                    GerakBumiMerah(-1);
                    GerakBumiBiru(0);
                    if (BumiBiru.transform.position == CenterCanvas.transform.position)
                    {
                        levelChosen++;
                        rightChecked = false;
                    }
                    break;

                case 2:
                    GerakBumiBiru(-1);
                    GerakBumiKuning(0);
                    if (BumiKuning.transform.position == CenterCanvas.transform.position)
                    {
                        levelChosen++;
                        rightChecked = false;
                    }
                    break;

                case 3:
                    GerakBumiKuning(-1);
                    GerakBumiHijau(0);
                    if (BumiHijau.transform.position == CenterCanvas.transform.position)
                    {
                        levelChosen++;
                        rightChecked = false;
                    }
                    break;

                case 4:
                    GerakBumiHijau(-1);
                    GerakBumiPink(0);
                    if (BumiPink.transform.position == CenterCanvas.transform.position)
                    {
                        levelChosen++;
                        rightChecked = false;
                    }
                    break;
            }
        }

        if (leftChecked)
        {
            switch (levelChosen)
            {
                case 2:
                    GerakBumiMerah(0);
                    GerakBumiBiru(1);
                    if (BumiMerah.transform.position == CenterCanvas.transform.position)
                    {
                        levelChosen--;
                        leftChecked = false;
                    }
                    break;

                case 3:
                    GerakBumiBiru(0);
                    GerakBumiKuning(1);
                    if (BumiBiru.transform.position == CenterCanvas.transform.position)
                    {
                        levelChosen--;
                        leftChecked = false;
                    }
                    break;

                case 4:
                    GerakBumiKuning(0);
                    GerakBumiHijau(1);
                    if (BumiKuning.transform.position == CenterCanvas.transform.position)
                    {
                        levelChosen--;
                        leftChecked = false;
                    }
                    break;

                case 5:
                    GerakBumiHijau(0);
                    GerakBumiPink(1);
                    if (BumiHijau.transform.position == CenterCanvas.transform.position)
                    {
                        levelChosen--;
                        leftChecked = false;
                    }
                    break;
            }
        }
    }

    void GerakBumiMerah(int value)
    {
        switch (value)
        {
            case 1:
                BumiMerah.transform.position = Vector3.Lerp(BumiMerah.transform.position, RightCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case 0:
                BumiMerah.transform.position = Vector3.Lerp(BumiMerah.transform.position, CenterCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case -1:
                BumiMerah.transform.position = Vector3.Lerp(BumiMerah.transform.position, LeftCanvas.transform.position, speedMove * Time.deltaTime);
                break;
        }
    }
    void GerakBumiBiru(int value)
    {
        switch (value)
        {
            case 1:
                BumiBiru.transform.position = Vector3.Lerp(BumiBiru.transform.position, RightCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case 0:
                BumiBiru.transform.position = Vector3.Lerp(BumiBiru.transform.position, CenterCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case -1:
                BumiBiru.transform.position = Vector3.Lerp(BumiBiru.transform.position, LeftCanvas.transform.position, speedMove * Time.deltaTime);
                break;
        }
    }
    void GerakBumiKuning(int value)
    {
        switch (value)
        {
            case 1:
                BumiKuning.transform.position = Vector3.Lerp(BumiKuning.transform.position, RightCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case 0:
                BumiKuning.transform.position = Vector3.Lerp(BumiKuning.transform.position, CenterCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case -1:
                BumiKuning.transform.position = Vector3.Lerp(BumiKuning.transform.position, LeftCanvas.transform.position, speedMove * Time.deltaTime);
                break;
        }
    }
    void GerakBumiHijau(int value)
    {
        switch (value)
        {
            case 1:
                BumiHijau.transform.position = Vector3.Lerp(BumiHijau.transform.position, RightCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case 0:
                BumiHijau.transform.position = Vector3.Lerp(BumiHijau.transform.position, CenterCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case -1:
                BumiHijau.transform.position = Vector3.Lerp(BumiHijau.transform.position, LeftCanvas.transform.position, speedMove * Time.deltaTime);
                break;
        }
    }
    void GerakBumiPink(int value)
    {
        switch (value)
        {
            case 1:
                BumiPink.transform.position = Vector3.Lerp(BumiPink.transform.position, RightCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case 0:
                BumiPink.transform.position = Vector3.Lerp(BumiPink.transform.position, CenterCanvas.transform.position, speedMove * Time.deltaTime);
                break;

            case -1:
                BumiPink.transform.position = Vector3.Lerp(BumiPink.transform.position, LeftCanvas.transform.position, speedMove * Time.deltaTime);
                break;
        }
    }
    
    void right()
    {
        if (levelChosen < 5)
        {
            rightChecked = true;
        }
    }
    void left()
    {
        if (levelChosen > 1)
        {
            leftChecked = true;
        }
    }
}
