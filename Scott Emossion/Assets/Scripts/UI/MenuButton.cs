﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuButton : MonoBehaviour
{
    [SerializeField] MenuButtonController menuButtonController;
    [SerializeField] Animator animator;
    [SerializeField] AnimatorFunctions animatorFunctions;
    [SerializeField] int thisIndex;
    [SerializeField] public Renderer bgButton;
    [SerializeField] public TextMeshProUGUI textButton;

    // Update is called once per frame
    void Update()
    {
        CheckButtonCondition();
    }

    private void LateUpdate()
    {
        //QuitGame();
    }

    private void CheckButtonCondition()
    {
        if (menuButtonController.index == thisIndex)
        {
            animator.SetBool("selected", true);
            if (Input.GetAxis("Submit") == 1)
            {
                animator.SetBool("press", true);
            }
            else if (animator.GetBool("press"))
            {
                animator.SetBool("press", false);
                animatorFunctions.disableOnce = true;
            }
            bgButton.material.color = new Color32(255, 255, 255, 255);
            textButton.color = new Color32(0, 0, 0, 255);
        }
        else
        {
            animator.SetBool("selected", false);
            bgButton.material.color = new Color32(0, 0, 0, 255);
            textButton.color = new Color32(255, 255, 255, 255);
        }
    }

    void QuitGame()
    {
        if (menuButtonController.index == 2 && Input.GetAxis("Submit") == 1)
        {
            Debug.Log("Quit");
        }
    }
}
