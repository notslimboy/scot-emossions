﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine;

public class StreamBackground : MonoBehaviour
{
    public RawImage targetVideo;
    public VideoPlayer sourceVideo;
    public AudioSource sourceAudio;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(playVideo());
    }

    IEnumerator playVideo()
    {
        sourceVideo.Prepare();
        WaitForSeconds waitFor = new WaitForSeconds(1);

        while (sourceVideo.isPrepared)
        {
            yield return waitFor;
            break;
        }

        targetVideo.texture = sourceVideo.texture;
        sourceVideo.Play();
        sourceAudio.Play();
    }
}
