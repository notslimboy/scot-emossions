﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [Header("Data Enemy")]
    public bool flipTerbang;
    public bool destroyByObject;
    public bool destroyByPlayer;
    public float moveSpeed;
    public int indexWaypoint;
    public GameObject groupingEnemy;
    public Transform[] posisi;

    [Header("Sprite Changer")]
    public bool needSprite;
    public SpriteRenderer renderer;
    public Sprite kananJalan;
    public Sprite kiriJalan;

    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        transform.position = posisi[indexWaypoint].position;
    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position,
                                                posisi[indexWaypoint].position,
                                                moveSpeed * Time.deltaTime);

        if (transform.position == posisi[indexWaypoint].position) 
        {
            indexWaypoint += 1;
        }

        if (indexWaypoint == posisi.Length)
        {
            indexWaypoint = 0;
        }

        if (indexWaypoint == 0 && needSprite)
        {
            renderer.sprite = kiriJalan;
        }
        else if (indexWaypoint == 1 && needSprite)
        {
            renderer.sprite = kananJalan;
        }

        if (indexWaypoint == 0 && flipTerbang)
        {
            renderer.flipX = true;
        }
        else if (indexWaypoint == 1 && flipTerbang)
        {
            renderer.flipX = false;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Player":
                if (destroyByPlayer)
                    Destroy(groupingEnemy);
                break;
            case "GrabableObject":
                if (destroyByObject)
                    Destroy(groupingEnemy);
                break;
        }
    }
}
