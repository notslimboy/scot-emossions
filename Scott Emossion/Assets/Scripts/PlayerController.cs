﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    public float jumpForce;
    public int jumpCounter;

    public Animator animatorPlayer;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        animatorPlayer.SetFloat("Horizontal", Input.GetAxis("Horizontal"));

        Vector3 horizontal = new Vector3(Input.GetAxis("Horizontal"), 0.0f, 0.0f);
        transform.position = transform.position + horizontal * moveSpeed * Time.deltaTime;

        if (jumpCounter < 2 && Input.GetKeyDown(KeyCode.UpArrow))
        {
            rb.velocity = Vector2.up * jumpForce;
            jumpCounter++;
        }

        if (jumpCounter != 0)
        {
            jumpForce = 3;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Ground":
                GroundChecking();
                break;
            case "GrabableObject":
                GroundChecking();
                break;
            case "EnemyTop":
                StompingEnemy();
                break;
            case "EnemySide":
                GameOver();
                break;
        }
    }

    private void GameOver()
    {
        Debug.Log("GameOver");
    }

    private void StompingEnemy()
    {
        rb.velocity = Vector2.up * jumpForce;
    }

    private void GroundChecking()
    {
        jumpCounter = 0;
        jumpForce = 5.5f;
    }
}
